# ! make
# $Id: Makefile,v 1.1 2022-10-31 17:47:00+01 radek Exp $
# $Source: /home/radek/st/src/md-tangle/src/Makefile,v $

STD	:= -std=c11 -g
STACK	:=
WARNS	:= -Wall -Wextra -pedantic -Wconversion
CFLAGS	:= $(STD) $(STACK) $(WARNS)

# Where the tangle binary resides?  In this case we are using the
# development version.
ORG_TANGLE=$(HOME)/st/src/org-tangle/src/org-tangle
MD_TANGLE=$(HOME)/st/src/md-tangle/src/md-tangle0

SOURCES	= md-tangle.c
OBJECTS	= $(SOURCES:.c=.o)
EXECUTABLES = md-tangle md-tangle0
orgs	= $(wildcard *.org)	# Documents are recognised by '.org' extension
mds	= $(wildcard *.md)	# Mark down documents
pdfs	= $(orgs:.org=.pdf)  $(mds:.md=.pdf)
htmls	= $(orgs:.org=.html) $(mds:.md=.html)
txts	= $(orgs:.org=.txt)  $(mds:.md=.txt)

output=.
VPATH = $(output)
PANDOC_OPTS= --tab-stop=8
PANDOC_PDF_OPTS	= $(PANDOC_OPTS) -V geometry:a4paper,margin=2cm \
	--number-sections --toc --toc-depth=4 \
	--highlight-style tango

default: all

all:	$(SOURCES) $(EXECUTABLES) $(txts) $(pdfs)
	@echo SOURCES: $(SOURCES)
	@echo EXECUTABLES: $(EXECUTABLES)
	@echo txts: $(txts)
	@echo pdfs: $(pdfs)

clean:
	rm -v $(OBJECTS) $(EXECUTALBES) Makefile.deps *~ $(txts)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

# Tangle C source files
md-tangle.c : md-tangle.md
	$(MD_TANGLE) md-tangle.md

# Bootstrap tangle
md-tangle0 : md-tangle0.c
	$(CC) $(LDFLAGS) md-tangle0.c -o $@

-include Makefile.deps
Makefile.deps:
	$(CC) $(CFLAGS) -MM *.[c] > Makefile.deps


### Rules

$(output)/%.txt: %.org
	pandoc $(PANDOC_TXT_OPTS) -o $@ $<

$(output)/%.pdf: %.org
	pandoc $(PANDOC_PDF_OPTS) -o $@ $<

$(output)/%.txt: %.md
	pandoc $(PANDOC_TXT_OPTS) -o $@ $<

$(output)/%.pdf: %.md
	pandoc $(PANDOC_PDF_OPTS) -o $@ $<

# ORG to TXT (UTF-8) transformation
# %.txt: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-ascii-export-to-ascii

# # ORG to PDF usign LaTeX export
# %.pdf: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-latex-export-to-pdf
# 	rm -v $(basename $@).tex

# # ORG to HTML
# %.html: %.org
# 	emacs $(EMACS_OPTS) --visit=$< --funcall org-html-export-to-html

#Eof:$Id: Makefile,v 1.1 2022-10-31 17:47:00+01 radek Exp $
