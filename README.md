# md-tangle

The Tangle program for Markdown files

Please see [md-tangle.md](https://gitlab.com/Radek.Hnilica/md-tangle/-/blob/main/md-tangle.md) instead.

# Getting started
To solve problem of egg & chicken, I provide program `md-tangle0.c` which can be bootstrap `md-tangle` from the Markdown file.
