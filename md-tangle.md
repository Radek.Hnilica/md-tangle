% MD Tangle
% Radek Hnilica
% $Id: md-tangle.md,v 1.2 2022-10-31 17:51:28+01 radek Exp $

# Introduction
This is simple **tangle** program in the way of prof. Donald E. Knuth tangle program for literal programming.
It is my own implementation of **tangle** for processing `.md` (_markdown_) files, and creating program(s) source files for compiling.
I've note, that I do not have or plan to do **weave** program.  For transforming of the _markdown_ source to any other format, primary pdf, I use **pandoc** tool.

This program reuses significant code from 
## Reasoning for new tool
I want tool which allow me to use literal programming with _markdown_ files.  I already made such tool for org files but I want have option for using _markdown_ in addition to _org_.

## Principe of working
The program works in one path.  It does:

- Read the _markdown_ file and for each code block create its representation in memory.
- Identify root code blocks which are starting points for the source files.
- For each such block create file and recursively fill with the text from code blocks.

## Requirements
Functional requirements:

- The program should allow to tangle multiple _markdown_ files.  These are specified as arguments to the program.
The code blocks written to files could be mangled with `#lines` directives so when compiling them the error is reported with line number in original _markdown_ file.
- Some attributes, like generating `#lines` can be switched off or changed in format by attributes in root code node.
- The references which use code block inside of another code block could be in different formats.  Original tangle used `<<name>>`, I allow also to use `{{name}}` and in more general specify starting and ending character sequence by the user.

Non-Functional requirements:

- I do not look at the performance.  In implementation I pick up the more easy solution in first.  (Get it working first, then improve if needed.)

## History

Version 0.0.1 - in progress

## Points, Ideas, ToDo list
1. When there is chunk whose attributes do not start with class name, report this as an error.
## Bugs

# The program

```{.c chunk=preprocessor-defines}
#define	VERSION	"0.1.0.1"
#define _DEFAULT_SOURCE
```

## Program File

```{.c chunk=md-tangle file=md-tangle.c line=yes}
/* $Id: md-tangle.md,v 1.2 2022-10-31 17:51:28+01 radek Exp $
 * $Source: /home/radek/st/src/md-tangle/src/md-tangle.md,v $
 */
{{preprocessor-defines}}
{{includes}}
{{forward-type-declarations}}
{{structure-definitions}}
{{function-prototypes}}
{{global-variables}}
{{functions}}
{{main}}
```

## Includes
We need to use plenty of standard functions in our program.  So we
need function prototypes & data structures.  These are defined in
header files, and we need to include these.

```{.c chunk=includes}
#include <assert.h>

#include <ctype.h>
#include <errno.h>
#include <stdbool.h>		/* `bool` boolean type, & true/false */
#include <stdio.h>		/* elementary I/O */
#include <stdlib.h>
#include <string.h>		/* string & memory functions */
```

# The `main`

```{.c chunk=main}
int
main(int argc, char *argv[])
{
	puts("md-tangle v" VERSION " " __DATE__ " " __TIME__);
	if (argc !=2) {
		fprintf(stderr, "ERR(%d): Missing file name of md file!\n",
			__LINE__);
		exit (1);
	}
	tangle_file(argv[1]);
	return 0;
}
```

# Reading `md` file
## Chunk structure & functions
### The structure
```{.c chunk=forward-type-declarations}
typedef struct chunks_s chunks_t;
```

```{.c chunk=structure-definitions}
struct chunks_s {
	chunks_t *next;		/* next chunk in linked list */
	char	 *lang;		/* programming language */
	char	 *name;		/* chunk name / identifier */
	lines_t	 *lines;	/* first line in list of lines */
	lines_t	 *last_line;	/* last line in list of lines */
	bool	  is_root;	/* root chunk flag, bound to file */
	char	 *file_name;	/* name of the file to create */
	bool	 gen_line;	/* output #line directives */
};
```

### Tangle File

```{.c chunk=function-prototypes}
void tangle_file(char *file_name);
```

```{.c chunk=functions}
void
tangle_file(char *file_name)
{
	fprintf(stderr, "DBG(%d):%s(file_name='%s')\n",
		__LINE__, __func__, file_name);
	read_md_file(file_name);
	for (chunks_t *chunk = chunk_list; chunk; chunk = chunk->next) {
		if (chunk->is_root) {
			fprintf(stdout, "INFO: Generate file '%s'\n",
				chunk->file_name);
			tangle_root_chunk(chunk, file_name);
		}
	}
}
```

### Detect chunk delimiters

```{.c chunk=function-prototypes}
bool is_chunk_begin(char *line, /*out*/char *c, /*out*/int *no);
bool is_chunk_end(char *line, char c, int no);
chunks_t *parse_chunk_title(char *line);
```

Check if the `line` is start of code chunk.  If it is, a `true` is returned and `c` and `no` are filled with values describing the chunk starting string.
```{.c chunk=functions}
bool
is_chunk_begin(char *line, /*out*/char *c, /*out*/int *no)
{
	char	ch;		/* chunk starting character */
	int	chno = 0;	/* no. of `ch` */
	if (*line == '`' || *line == '~') {
		ch = *line;
		for (chno = 0; line[chno] == ch; chno++); /* counting ch */
		if (chno >= 3) {
			*c = ch; *no = chno;
			return true;
		}
	}
	return false;
}
```

Check if the `line` ends the code chunk.  The ending line must have
the same number of `c` characters `no` as the chunk starting line.

```{.c chunk=functions}
bool
is_chunk_end(char *line, char c, int no)
{
	int	chno = 0;
	for (chno = 0; line[chno] == c; chno++);
	if (chno == no) return true;
	return false;
}
```

### Parse title of the chunk

Parse the chunk starting line and fill the information to newly
created `chunk_t` which is then returned.
```{.c chunk=function-prototypes}
chunks_t *parse_chunk_title(char *line);
```

```{.c chunk=global-variables}
char	*p = NULL;		/* Pointer to parsing text */
```

```{.c chunk=functions}
chunks_t *
parse_chunk_title(char *line)
{
	chunks_t *chunk = NULL;

	p = line;		/* parsing pointer */
	while (isblank(*p)) p++; /* skip blanks */
	if (expect('{')) {
		chunk = (chunks_t*) calloc(1, sizeof(chunks_t));
		memset(chunk, 0, sizeof(chunks_t));
		parse_list(chunk);	//parse list p+1
		/* The title ending '}' is eaten by parse_list */
	}
	return chunk;
}
```

## Lines structure & functions

```{.c chunk=forward-type-declarations}
typedef struct lines_s lines_t;
```

```{.c chunk=structure-definitions}
struct lines_s {
	lines_t	*next;
	int	lineno;
	char	*text;
};
```
## Parsing class & attributes list
Expect character `c`.  If character do not match then report
failure and return false.
```{.c chunk=global-variables}
char	*p;			/* pointer to string in parsing */
```

~~~~{.c chunk=function-prototypes}
bool expect(char c);
~~~~

~~~~ { .c chunk=functions }
bool expect(char c)
{
	if (*p == c) {
		p++;
		return true;
	}
	else {
		fprintf(stderr, "ERR(%d): expected '%c' got '%c' on line %d!\n",
			__LINE__, c, *p, lineno);
		return false;
	}
}
~~~~

Parse whitespace separated list of classes & attributes.  Fill the
well known into chunk attributes.
```{.c chunk=function-prototypes}
bool parse_list(chunks_t *chunk);
```

```{.c chunk=functions}
bool parse_list(chunks_t *chunk)
{
	char	*class;
	char	*attr, *value;
	bool	first_class = true;

	while (isblank(*p)) p++; /* skip blanks */
	while ((*p != '}') && (*p != '\0')) {
		//fprintf(stderr, "DBG(%d): *p is '%s'\n", __LINE__, p);
		if (*p == '.') {
			p++;	/* eat the '.' */
			class = strsep(&p, " \t}");
			if (first_class) {
				chunk->lang = strdup(class);
				first_class = false;
			}
			else {
				fprintf(stderr, "ERR(%d) unknown class='%s'\n", __LINE__, class);

			}
		}
		else if (isalpha(*p)) {
			attr = strsep(&p, "=");
			//fprintf(stderr, "INFO(%d) rest='%s'\n", __LINE__, p);
			value = strsep(&p, " \t0}");

			if (strcmp("chunk", attr) == 0) {
				chunk->name = strdup(value);
			}
			else if (strcmp("file", attr) == 0) {
				chunk->file_name = strdup(value);
				chunk->is_root = true;
			}
			else if (strcmp("line", attr) == 0) {
				if (strcmp("no", value) == 0)
					chunk->gen_line = false;
				else
					chunk->gen_line = true;
			}
			else {
				fprintf(stderr, "INFO(%d) unknown %s='%s'\n", __LINE__, attr, value);
			}
		}
		while (isblank(*p)) p++; /* skip blanks */
	}
	return true;
}
```

# Read MD file
Given the name of the _markdown_ file this procedure reads the file
and returns pointer to list of code chunks found in that file.

The returned value is not necessary as the list is in global variable.
The global variable is a must because while reading the code chunks we
need to consult the list of already read ones.  This is because when
the chunk name is seen for second and other times it do not create new
chunk but extend existing one.

```{.c chunk=global-variables}
chunks_t *chunk_list;
char	  lbuf[256];
int	  lineno = 0;
```

```{.c chunk=function-prototypes}
void read_md_file(char *file_name);
```

```{.c chunk=functions}
void
read_md_file(char *file_name)
{
	FILE	 *ifh;
	chunks_t *chunk = NULL;
	bool	  in_chunk = false;
	char	  ch;
	int	  chno;


	lineno = 0; /* reset the line counter as we are opening new file */
	if ((ifh = fopen(file_name, "r")) == NULL) {
		fprintf(stderr, "ERR(%d): Can't open input file %s.  %s.",
			__LINE__, file_name, strerror(errno));
	}
	while (!feof(ifh)) {
		memset(lbuf, 0, sizeof(lbuf));
		if (!fgets(lbuf, sizeof(lbuf), ifh)) break;
		rtrim(lbuf, '\n');
		lineno++;

		if (in_chunk) {
			if (is_chunk_end(lbuf, ch, chno)) {
				in_chunk = false;
			}
			else {
				{{add-line-to-chunk}}
			}
		}
		else {
			/* Text out of the code chunk */
			if (is_chunk_begin(lbuf, &ch, &chno)) {
				chunk = parse_chunk_title(&lbuf[chno]);
				assert(chunk);
				chunks_t *c = find_chunk(chunk->name);
				//fprintf(stderr, "DBG(%d) c=%p, name=%s\n", __LINE__, (void*)c, chunk->name);
				if (c) {
					//fprintf(stderr, "DBG(%d)\n", __LINE__);
					free(chunk);
					chunk = c;
				}
				else {
					/* Add new chunk to list */
					chunk->next = chunk_list;
					chunk_list = chunk;
				}				
				in_chunk = true;
			}
			else {
				/* Ignore text out of code chunk */
			}
		}
	}
}
```

```{.c chunk=add-line-to-chunk}
lines_t *line = (lines_t *) calloc(1, sizeof(lines_t));
line->text = strndup(lbuf, sizeof(lbuf));
line->lineno = lineno;
line->next = NULL;
if (chunk->lines == NULL)
	chunk->lines = chunk->last_line = line;
else {
	chunk->last_line->next = line;
	chunk->last_line = line;
}
```

# Tangling
## Tangle Root Chunk
```{.c chunk=function-prototypes}
void
tangle_root_chunk(chunks_t *chunk, char *md_file_name);
```

Tangle root chunk is a wrapper for `tangle_chunk`.  It setup the
environment by opening the file to be generated, handle some
settings and the run `tangle_chunk` with correct arguments.

```{.c chunk=functions}
void
tangle_root_chunk(chunks_t *chunk, char *md_file_name)
{
	FILE *f;

	assert(chunk);
	assert(chunk->file_name);
	f = fopen(chunk->file_name, "w");
	if (f == NULL) {
		fprintf(stderr, "ERR(%d): Can't open output file %s. %s.\n",
			__LINE__, chunk->file_name, strerror(errno));
		exit (1);
	}
	if (chunk->gen_line) {
		fprintf(f, "#line 1 \"%s\"\n", md_file_name);
	}
	tangle_chunk(chunk, chunk, "", f);
	fclose(f);
}
```
## Tangle Chunk
```{.c chunk=function-prototypes}
void
tangle_chunk(chunks_t *chunk, chunks_t *root, char *indent, FILE *f);
```

Tangle chunk into opened file.  This procedure need to know not only the chunk itself, but for the context also:

* root chunk -- there are some global settings for the file
* indent     -- the chunk needs to be properly indented
* f -- is the FILE / STREAM to write the chunk code into.

```{.c chunk=functions}
void
tangle_chunk(chunks_t *chunk, chunks_t *root, char *indent, FILE *f)
{
	lines_t *line;
	int	lineno = 0;

	//fprintf(stderr, "INFO(%d) tangle {{%s}}\n", __LINE__, chunk->name);
	for (line = chunk->lines; line; line = line->next) {
		if (root->gen_line && line->lineno != lineno+1) {
			fprintf(f, "#line %d\n", line->lineno);
		}
		tangle_line(line, root, indent, f);
		lineno = line->lineno;
	}
}
```

## Tangle Chunk by Name
```{.c chunk=function-prototypes}
void tangle_chunk_by_name(char *name, chunks_t *root, char *indent, FILE *f);
chunks_t *find_chunk(char *name);
```

```{.c chunk=functions}
void
tangle_chunk_by_name(char *name, chunks_t *root, char *indent, FILE *f)
{
	chunks_t *chunk = find_chunk(name);
	if (!chunk) {
		fprintf(stderr, "ERR(%d): chunk %s not found!  On line %d.\n",
			__LINE__, name, lineno);
		return;
	}
	tangle_chunk(chunk, root, indent, f);
}
```

```{.c chunk=functions}
chunks_t *
find_chunk(char *name)
{
	if (!name) return NULL;
	for (chunks_t *chunk = chunk_list; chunk != NULL; chunk = chunk->next) {
		if (chunk->is_root) continue;
		if (!chunk->name) continue;
		if (strcmp(chunk->name, name) == 0) return chunk;
	}
	return NULL;
}
```

## Tangle Source Code Line
```{.c chunk=function-prototypes}
void tangle_line(lines_t *line, chunks_t *root, char *indent, FILE *f);
```

```{.c chunk=functions}
void
tangle_line(lines_t *line, chunks_t *root, char *indent, FILE *f)
{
	char *p = line->text;
	char *t = NULL;
	while(isblank(*p) && *p != '\0') p++; /* skip blanks */
	if (strncmp(p, "{{", 2) == 0) {
		if ((t = strstr(p, "}}")) != NULL) {
			size_t l = (size_t)(t - p);
			l = l - 2;
			char *ref = strndup(p+2, l);
			tangle_chunk_by_name(ref, root, indent, f);
		}
	}
	else {
		fprintf(f, "%s%s\n", indent, line->text);
	}
}
```

# Helper Functions
## Trim Line from Right
Removing unwanted, `junk`, characters from right side of the line.

```{.c chunk=function-prototypes}
char *rtrim(char* string, char junk);
```

```{.c chunk=functions}
char *
rtrim(char* string, char junk)
{
        char* original = string + strlen(string);
        while (*--original == junk); /* look for first non junk character */
        *(original + 1) = '\0';      /* replace last junk character */
        return string;
}
```


# System ROM for 6502 CPU
This code is for system page which for 6502 CPU is `$F000`-`$FFFF`.  It contain or should contain:

- start-up & initialization code for core hardware
- Interface/API to core hardware (this means that programs are supposed to use calls to interact with core hardware, thus making them hardware independent.)
- Elementary monitor program to examine, and modify memory and run code.
- Extended commands for monitor to allow load/save and other functions.
- Library of useful functions for printing strings and others.
- Possible implementation of Steve Wozniak Sweet 16.
- Possible assembler extensions to the monitor.


~~~{.asm .numberLines myattr=something}
START:
	LDA	#23
	STA	CIO
	JSR	SYS
~~~

```{.C chunk=aloha}
int main(int argc, char *argv[])
{
	puts("Hello sailor!");
}
```

| Id  | Language  | Description                  |
|:----|:---------:|:-----------------------------|
| C   | C lang    | Old good C                   |
| A65 | Assembler | for 6502 cpu                 |
| A68 | Assembler | for 6800 cpu                 |
| A69 | Assembler | for 6809/6309 cpu            |
| Z80 | Assembler | for Z80, Z180 family of cpus |


```{.sh chunk=shell lines=no lines=yes}
cd somewhere
ls -l there
```

