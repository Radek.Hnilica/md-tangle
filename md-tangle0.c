/*
 * $Id: md-tangle0.c,v 1.2 2022-10-31 17:53:20+01 radek Exp radek $
 * $Source: /home/radek/st/src/md-tangle/src/md-tangle0.c,v $
 * Bootstrap version of md-tangle.
 */
#define VERSION "0.0.3"
#define _DEFAULT_SOURCE

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>	      /* Header-file for boolean data-type. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Forward definition of types */
typedef struct chunk_s chunks_t;
typedef struct lines_s lines_t;

/* Linked list of code chunks */
struct chunk_s {
	chunks_t *next;	/* next chunk in linked list */
	char	 *lang;	/* programming language */
	char	 *name; /* chunk name / identifier */
	lines_t	 *lines;
	lines_t	 *last_line;
	bool	  is_root;	/* root chunk flag, bound to file */

	char	 *file_name;
	bool	  gen_line_directive; /* output #line */
};

/* Linked list of lines */
struct lines_s {
	lines_t	*next;		/* single linked list */
	int	 lineno;	/* line # in original md file */
	char	*text; 		/* content of the line without trailing '\n' */
};

/* Function prototypes */
void	  tangle_file(char *name);
void	  read_md_file(char *file_name);
bool	  is_chunk_begin(char *line, /*out*/char *c, /*out*/int *no);
bool	  is_chunk_end(char *line, char c, int no);
chunks_t *parse_chunk_title(char *line);
char	 *rtrim(char* string, char junk);
bool	  expect(char c);
bool	  match(char c);
bool	  parse_list(chunks_t *chunk);
void	  tangle_root_chunk(chunks_t *chunk, char *md_file_name);
void	  tangle_chunk(chunks_t *chunk, chunks_t *root, char *indent, FILE *f);
void	  tangle_chunk_by_name(char *name, chunks_t *root, char *indent, FILE *f);
chunks_t *find_chunk(char *name);
void	  tangle_line(lines_t *line, chunks_t *root, char *indent, FILE *f);
void	  dbg_chunks(chunks_t *chunks);

/* Global variables */
chunks_t *chunk_list;
char	  lbuf[256];
int	  lineno = 0;

/*
 * Read markdown file and return list of code chunks found in that file.
 */
void
read_md_file(char *file_name)
{
	FILE	 *ifh;
	chunks_t *chunk = NULL;
	bool	  in_chunk;
	char	  ch;
	int	  chno;

	ifh = fopen(file_name, "r");
	lineno = 0;
	while (!feof(ifh)) {
		memset(lbuf, 0, sizeof(lbuf));
		if (!fgets(lbuf, sizeof(lbuf), ifh)) break;
		rtrim(lbuf, '\n');
		lineno++;

		//fprintf(stderr, "DBG(%d): line %s\n", __LINE__, lbuf);
		// read_line with skip to EOL

		if (in_chunk) {
			if (is_chunk_end(lbuf, ch, chno)) {
				in_chunk = false;
			}
			else {
				// Append line to chunk
				lines_t *line = (lines_t *) calloc(1, sizeof(lines_t));
				line->text = strndup(lbuf, sizeof(lbuf));
				line->lineno = lineno;
				line->next = NULL;
				if (chunk->lines == NULL)
					chunk->lines = chunk->last_line = line;
				else {
					chunk->last_line->next = line;
					chunk->last_line = line;
				}
			}
		}
		else {/* not in chunk */
			// Chunk start?, get start mark
			if (is_chunk_begin(lbuf, &ch, &chno)) {
				chunk = parse_chunk_title(&lbuf[chno]);
				assert(chunk);
				/*FIXME: we can't create the chunk if we do not know this is first use of the chunk name! */
				/* Do we already know that chunk  */
				/* search for chunk of the same name in between known chunks */
				chunks_t *c = find_chunk(chunk->name);
				//fprintf(stderr, "DBG(%d) c=%p, name=%s\n", __LINE__, (void*)c, chunk->name);
				if (c) {
					//fprintf(stderr, "DBG(%d)\n", __LINE__);
					free(chunk);
					chunk = c;
				}
				else {
					/* Add new chunk to list */
					chunk->next = chunk_list;
					chunk_list = chunk;
				}				
				in_chunk = true;
			}
			else {
				// Ignore line
			}
		}
	}
	return chunk_list;
}


/* **************************************************************
 * Chunk functions
 */
/*
 * Check if the `line` is start of code chunk.  If it is, a `true` is
 * returned and `c` and `no` are filled with values describing the
 * chunk starting string.
 */
bool is_chunk_begin(char *line, /*out*/char *c, /*out*/int *no)
{
	char	ch;		/* chunk starting character */
	int	chno = 0;	/* no. of `ch` */
	if (*line == '`' || *line == '~') {
		ch = *line;
		for (chno = 0; line[chno] == ch; chno++); /* counting ch */
		if (chno >= 3) {
			*c = ch; *no = chno;
			return true;
		}
	}
	return false;
}

/*
 * Check if the `line` ends the code chunk.  The ending linu must have
 * the same number of `c` characters `no` as the chunk starting line.
 */
bool is_chunk_end(char *line, char c, int no)
{
	int	chno = 0;
	for (chno = 0; line[chno] == c; chno++);
	if (chno == no) return true;
	return false;
}

/*
 * Parse the chunk starting line and fill the information to newly
 * created chunk_t which is then returned.
 */
char	*p = NULL;		/* Pointer to parsing text */
chunks_t *parse_chunk_title(char *line)
{
	chunks_t *chunk = NULL;
	//fprintf(stderr, "DBG(%d):%s(title='%s')\n", __LINE__, __func__, line);

	p = line;		/* parsing pointer */
	while (isblank(*p)) p++; /* skip blanks */
	if (expect('{')) {
		chunk = (chunks_t*) calloc(1, sizeof(chunks_t));
		memset(chunk, 0, sizeof(chunks_t));
		parse_list(chunk);	//parse list p+1
		/* The title ending '}' is eaten by parse_list */
	}
	return chunk;
}


/* **************************************************************
 * Atributes and class list parser
 */

/* Expect character `c`.  If character do not match then report
 * failure and return false.
 */
bool expect(char c)
{
	//fprintf(stderr, "DBG(%d):%s(c='%c')\n", __LINE__, __func__, c);
	if (*p == c) {
		p++;
		return true;
	}
	else {
		fprintf(stderr, "ERR(%d): expected '%c' got '%c' on line %d!\n",
			__LINE__, c, *p, lineno);
		return false;
	}
}

bool match(char c)
{
	return (*p == c);
}

/* Parse whitespace separated list of classes & attributes.  Fill the
 * well known into chunk attributes.
 */
bool parse_list(chunks_t *chunk)
{
	char	*class;
	char	*attr, *value;
	bool	first_class = true;

	while (isblank(*p)) p++; /* skip blanks */
	while ((*p != '}') && (*p != '\0')) {
		if (match ('.')) {
			p++;	/* eat the '.' */
			class = strsep(&p, " \t}");
			if (first_class) {
				chunk->lang = strdup(class);
				first_class = false;
			}
			else {
				fprintf(stderr, "ERR(%d) unknown class='%s'\n", __LINE__, class);

			}
		}
		else if (isalpha(*p)) {
			attr = strsep(&p, "=");
			//fprintf(stderr, "INFO(%d) rest='%s'\n", __LINE__, p);
			value = strsep(&p, " \t0}");

			if (strcmp("chunk", attr) == 0) {
				chunk->name = strdup(value);
			}
			else if (strcmp("file", attr) == 0) {
				chunk->file_name = strdup(value);
				chunk->is_root = true;
			}
			else if (strcmp("line", attr) == 0) {
				if (strcmp("no", value) == 0)
					chunk->gen_line_directive = false;
				else
					chunk->gen_line_directive = true;
			}
			else {
				fprintf(stderr, "INFO(%d) unknown %s='%s'\n", __LINE__, attr, value);
			}
		}
		while (isblank(*p)) p++; /* skip blanks */
	}
	return true;
}
	


void
tangle_file(char *file_name)
{
	fprintf(stderr, "DBG(%d):%s(file_name='%s')\n", __LINE__, __func__, file_name);
	read_md_file(file_name);
	//dbg_chunks(chunk_list);
	//chunk_list = chunks;	/* This is needed by find_chunk */
	for (chunks_t *chunk = chunk_list; chunk; chunk = chunk->next) {
		if (chunk->is_root) {
			fprintf(stdout, "INFO(%d): Generate file '%s'\n",
				__LINE__, chunk->file_name);
			tangle_root_chunk(chunk, file_name);
		}
	}
}

/* Tangle root chunk is a wrapper for tangle_chunk.  It setup the
 * environment by opening the file to be generated, handle some
 * settings and the run tangle_chunk with correct arguments.
 */
void
tangle_root_chunk(chunks_t *chunk, char *md_file_name)
{
	FILE *f;

	fprintf(stderr, "DBG(%d) %s()\n", __LINE__, __func__);
	assert(chunk);
	assert(chunk->file_name);
	f = fopen(chunk->file_name, "w");
	if (f == NULL) {
		fprintf(stderr, "ERR: Can't open output file %s. %s.\n",
			chunk->file_name, strerror(errno));
		exit (1);
	}
	if (chunk->gen_line_directive) {
		fprintf(f, "#line 1 \"%s\"\n", md_file_name);
	}
	tangle_chunk(chunk, chunk, "", f);
	fclose(f);
}

/*
 * Tangle chunk into opened file.  This procedure need to know not only the chunk itself, but for the context also:
 * root chunk -- there are some global settings for the file
 * indent     -- the chunk needs to be properely indented
 * f -- is the FILE / STREAM to write the chunk code into.
 */
void
tangle_chunk(chunks_t *chunk, chunks_t *root, char *indent, FILE *f)
{
	lines_t *line;
	int	lineno = 0;

	//fprintf(stderr, "INFO(%d) tangle {{%s}}\n", __LINE__, chunk->name);
	//fprintf(f, "/*{{%s}}*/\n", chunk->name);
	for (line = chunk->lines; line; line = line->next) {
		if (root->gen_line_directive && line->lineno != lineno+1) {
			fprintf(f, "#line %d\n", line->lineno);
		}
		tangle_line(line, root, indent, f);
		lineno = line->lineno;
	}
}

void
tangle_chunk_by_name(char *name, chunks_t *root, char *indent, FILE *f)
{
	chunks_t *chunk = find_chunk(name);
	//fprintf(stderr, "DBG(%d) name=%s, %p\n", __LINE__, name, (void*)chunk);
	if (!chunk) {
		fprintf(stderr, "ERR(%d): chunk %s not found!  On line %d.\n",
			__LINE__, name, lineno);
		return;
	}
	tangle_chunk(chunk, root, indent, f);
}

chunks_t *
find_chunk(char *name)
{
	if (!name) return NULL;
	for (chunks_t *chunk = chunk_list; chunk != NULL; chunk = chunk->next) {
		if (chunk->is_root) continue;
		if (!chunk->name) continue;
		if (strcmp(chunk->name, name) == 0) return chunk;
	}
	return NULL;
}

void
tangle_line(lines_t *line, chunks_t *root, char *indent, FILE *f)
{
	char *p = line->text;
	char *t = NULL;
	while(isblank(*p) && *p != '\0') p++; /* skip blanks */
	if (strncmp(p, "{{", 2) == 0) {
		if ((t = strstr(p, "}}")) != NULL) {
			int l = t - p - 2;
			char *ref = strndup(p+2, l);
			tangle_chunk_by_name(ref, root, indent, f);
		}
	}
	else {
		fprintf(f, "%s%s\n", indent, line->text);
	}
}


/*
 * trim character from end of the string
 */
char* rtrim(char* string, char junk)
{
        char* original = string + strlen(string);
        while (*--original == junk); /* look for first non junk character */
        *(original + 1) = '\0';      /* replace last junk character */
        return string;
}


/* **************************************************************
 * Debug helpers
 */
/* Print list of chunks, each chunk by title */
void
dbg_chunks(chunks_t *chunks)
{
	fprintf(stdout, "List of all chunks\n");
	for (chunks_t *chunk = chunks; chunk != NULL; chunk = chunk->next) {
		fprintf(stdout, ":(%p) '%s'\n", (void*)chunk, chunk->name);
		for (lines_t *line = chunk->lines; line; line = line->next) {
			fprintf(stdout, "  %.4d: %s\n", line->lineno, line->text);
		}
	}
}

int
main(int argc, char *argv[])
{
	puts("md-tangle0 v" VERSION " " __DATE__ " " __TIME__);

	if (argc != 2) {
		fprintf(stderr, "Missing file name of md file!\n");
		exit (1);
	}
	tangle_file(argv[1]);
	return 0;
}
